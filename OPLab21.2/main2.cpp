#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <windows.h>

int main() {

	char str[100];
	gets_s(str);
	strcat(str, " ");
	int min = 100, max = 0;

	int k = 0;
	for (int i = 0; i < strlen(str); i++) {
		if (isspace(str[i])) {
			if(k > max) max = k;
			if(k < min) min = k;
			k = 0;
			continue;
		}
		k++;
	}
	for (int i = 0; i < strlen(str); i++) {
		if (isspace(str[i])) {
			if (k == max) {
				printf("The longest word: ");
				for (int j = i - max; j < i; j++) printf("%c", str[j]);
				printf("\n");
			}
			if (k == min) {
				printf("The shortest word: ");
				for (int j = i - min; j < i; j++) printf("%c", str[j]);
				printf("\n");
			}
			k = 0;
			continue;
		}
		k++;
	}


	return 0;
}