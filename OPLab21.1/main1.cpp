#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <windows.h>

int main() {

	char str[100];
	gets_s(str);
	
	for (int i = 0; i < strlen(str); i++) {
		if (islower(str[i])) str[i] = toupper(str[i]);
		else str[i] = tolower(str[i]);
	}
	puts(str);
	return 0;
}