#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <windows.h>

void deleteSpaces(char string[]) {
	char* result = (char*)malloc(strlen(string) + 1);
	int resultIndex = 0;

	for (int i = 0; i < strlen(string); i++) {
		if (!(isspace(string[i]) && isspace(string[i - 1]))) {
			result[resultIndex] = string[i];
			resultIndex++;
		}
	}
	result[resultIndex] = '\0';
	strcpy(string, result);
	free(result);
}


int countWords(char string[]) {
	int count = 0;
	for (int i = 0; i < strlen(string); i++) {
		if (isspace(string[i])) count++;
	}
	return count;
}

char* theBiggestVowelCount(char string[]) {
	int count = 0, max = 0, countLetters = 0, maxCountLetters = 0;
	for (int i = 0; i < strlen(string); i++) {
		if (isspace(string[i])) {
			if (count > max) {
				max = count;
				maxCountLetters = countLetters;
			}
			count = 0;
			countLetters = 0;
			continue;
		}
		switch (string[i]) {
		case 'A':
		case 'a':
		case 'e':
		case 'E':
		case 'I':
		case 'i':
		case 'O':
		case 'o':
		case 'U':
		case 'u':
		case 'Y':
		case 'y':
			count++;
		}
		countLetters++;
	}

	char* str = (char*) calloc(maxCountLetters + 1, sizeof(char));
	str[maxCountLetters] = '\0';

	for(int i = 0; i < strlen(string); i++){
		if (isspace(string[i])) {
			if (count == max) {
				int k = 0;
				for (int j = i - maxCountLetters; j < i; j++, k++) str[k] = string[j];
				return str;
			}
			count = 0;
			continue;
		}
		switch (string[i]) {
		case 'A':
		case 'a':
		case 'e':
		case 'E':
		case 'I':
		case 'i':
		case 'O':
		case 'o':
		case 'U':
		case 'u':
		case 'Y':
		case 'y':
			count++;
		}
	}

}

int main() {

	char str[100];
	char* strP = str;
	gets_s(str);
	strcat(str, " ");

	deleteSpaces(str);
	puts(str);

	printf("\nCount of words = %d\n", countWords(str));

	char* biggestVowels = theBiggestVowelCount(str);
	printf("The word with the most vowels:\n");
	puts(theBiggestVowelCount(str));

	return 0;
}