#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <windows.h>

void playSound(char morz[]) {
	for (int i = 0; i < strlen(morz); i++) {
		switch (morz[i]) {
		case '.':
			Beep(1000, 200);
			break;
		case '-':
			Beep(1000, 600);
			break;
		case '\t':
			Sleep(600);
			break;
		}
	}
}

void morze(char string[]) {
	for (int i = 0; i < strlen(string); i++) {
		switch (string[i]){
		case 'A':
		case 'a':
		{
			char morze[] = ".-\t";
			printf(".-\t"); playSound(morze);
			break; 
		}
		case 'B':
		case 'b':
		{
			char morze[] = "-...\t";
			printf("-...\t"); playSound(morze);
			break;
		}
		case 'C':
		case 'c':
		{
			char morze[] = "-.-.\t";
			printf("-.-.\t"); playSound(morze);
			break;
		}
		case 'D':
		case 'd':
		{
			char morze[] = "-..\t";
			printf("-..\t"); playSound(morze);
			break;
		}
		case 'E':
		case 'e':
		{
			char morze[] = ".\t";
			printf(".\t"); playSound(morze);
			break;
		}
		case 'F':
		case 'f':
		{
			char morze[] = "..-.\t";
			printf("..-.\t"); playSound(morze);
			break;
		}
		case 'G':
		case 'g':
		{
			char morze[] = "--.\t";
			printf("--.\t"); playSound(morze);
			break;
		}
		case 'H':
		case 'h':
		{
			char morze[] = "....\t";
			printf("....\t"); playSound(morze);
			break;
		}
		case 'I':
		case 'i':
		{
			char morze[] = "..\t";
			printf("..\t"); playSound(morze);
			break;
		}
		case 'J':
		case 'j':
		{
			char morze[] = ".---\t";
			printf(".---\t"); playSound(morze);
			break;
		}
		case 'K':
		case 'k':
		{
			char morze[] = "-.-\t";
			printf("-.-\t"); playSound(morze);
			break;
		}
		case 'L':
		case 'l':
		{
			char morze[] = ".-..\t";
			printf(".-..\t"); playSound(morze);
			break;
		}
		case 'M':
		case 'm':
		{
			char morze[] = "--\t";
			printf("--\t"); playSound(morze);
			break;
		}
		case 'N':
		case 'n':
		{
			char morze[] = "-.\t";
			printf("-.\t"); playSound(morze);
			break;
		}
		case 'O':
		case 'o':
		{
			char morze[] = "---\t";
			printf("---\t"); playSound(morze);
			break;
		}
		case 'P':
		case 'p':
		{
			char morze[] = ".--.\t";
			printf(".--.\t"); playSound(morze);
			break;
		}
		case 'Q':
		case 'q': 
		{
			char morze[] = "--.-\t";
			printf("--.-\t"); playSound(morze);
			break;
		}
		case 'R':
		case 'r':
		{
			char morze[] = ".-.\t";
			printf(".-.\t"); playSound(morze);
			break;
		}
		case 'S':
		case 's':
		{
			char morze[] = "...\t";
			printf("...\t"); playSound(morze);
			break;
		}
		case 'T':
		case 't':
		{
			char morze[] = "-\t";
			printf("-\t"); playSound(morze);
			break;
		}
		case 'U':
		case 'u': 
		{
			char morze[] = "..-\t";
			printf("..-\t"); playSound(morze);
			break;
		}
		case 'V':
		case 'v':
		{
			char morze[] = "...-\t";
			printf("...-\t"); playSound(morze);
			break;
		}
		case 'W':
		case 'w':
		{
			char morze[] = ".--\t";
			printf(".--\t"); playSound(morze);
			break;
		}
		case 'X':
		case 'x':
		{
			char morze[] = "-..-\t";
			printf("-..-\t"); playSound(morze);
			break;
		}
		case 'Y':
		case 'y':
		{
			char morze[] = "-.--\t";
			printf("-.--\t"); playSound(morze);
			break;
		}
		case 'Z':
		case 'z':
		{
			char morze[] = "--..\t";
			printf("--..\t"); playSound(morze);
			break;
		}
		case '1':
		{
			char morze[] = ".----\t";
			printf(".----\t"); playSound(morze);
			break;
		}
		case '2':
		{
			char morze[] = "..---\t";
			printf("..---\t"); playSound(morze);
			break;
		}
		case '3':
		{
			char morze[] = "...--\t";
			printf("...--\t"); playSound(morze);
			break;
		}
		case '4':
		{
			char morze[] = "....-\t";
			printf("....-\t"); playSound(morze);
			break;
		}
		case '5':
		{
			char morze[] = ".....\t";
			printf(".....\t"); playSound(morze);
			break;
		}
		case '6':
		{
			char morze[] = "-....\t";
			printf("-....\t"); playSound(morze);
			break;
		}
		case '7':
		{
			char morze[] = "--...\t";
			printf("--...\t"); playSound(morze);
			break;
		}
		case '8':
		{
			char morze[] = "---..\t";
			printf("---..\t"); playSound(morze);
			break;
		}
		case '9':
		{
			char morze[] = "----.\t";
			printf("----.\t"); playSound(morze);
			break;
		}
		case '0':
		{
			char morze[] = "-----\t";
			printf("-----\t"); playSound(morze);
			break;
		}
		default:
		{
			char morze[] = "\t\t\t";
			printf("\t\t\t"); playSound(morze);
			break;
		}
		}
	}
}

int main() {
	char str[100];
	char* strP = str;
	gets_s(str);
	
	morze(str);
	return 0;
}